### CALCULATION METHOD   
$OrchVegRatio= \frac{orchards \:and\: vegetable\: farming\: area\: }{SPU\: area},  [-];$  

### DESCRIPTION    
Orchards & vegetable farming area to SPU area ratio.

### GROUP   
general, drought, flood and quality  

