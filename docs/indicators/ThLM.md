It is a method for identifying different drought characteristics (number of droughts in multi-annual period, their depths and durations) for further analysis. General formula: X<X_Threshold
                  It can be applied to several hydro- & meteorological variables, e.g. flows, SPI, CWB etc.
                  TBD: i) which hydro-meteo variables and ii) which drought characteristics will be analysed; iii) threshold type (constant value, seasonal values, monthly/weekly/daily thresholds), iv) threshold values (80% exceedence percentile?)
