
### CALCULATION METHOD  
$EcoAraBuf20mRatio= \frac{arable\: lands\: in\: buffers\: around\: surface\: waters  \: area\: }{SPU\: area},  [-];$  

### DESCRIPTION   
Arable lands in 20-meters buffer around surface waters (streams, rivers and lakes) area to SPU area ratio.

### GROUP  
general and quality  

