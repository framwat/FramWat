
### CALCULATION METHOD  
$FlowMinMaxRatio=  \frac {swMLQ}{ swMHQ}, [-];$  
where:  
swMLQ - mean low flow   
swMHQ - mean high flow 

### DESCRIPTION  
Variability of river flows in the multiannual period; Caculated for internal runoff from sub-basins.

### GROUP  
general, drought and flood 

