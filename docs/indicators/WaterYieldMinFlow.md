### CALCULATION METHOD  
$WaterYieldMinFlow, [mm];$  

### DESCRIPTION  
Water yield (specific runoff) for low flow [swMLQ]

### GROUP  
general and drought 

