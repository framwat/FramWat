
### CALCULATION METHOD  
$grr[mm]$   

### DESCRIPTION  
Groundwater Renewable Resources Module. Prepare a ready map from the hydrogeological institute or from the hydrogeological map.  

### GROUP  
general and drought  

