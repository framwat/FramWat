
### CALCULATION METHOD  
$NonForestedRatio= \frac{Non\:forested\: areas\: with\: slope\: above\: 5\%\; }{SPU\: area},  [-];$  

### DESCRIPTION  
NonForestedRatio- Non forested areas with a slope above 5% (2,86°; 0.05 radian) to SPU area ratio.

### GROUP  
general, flood and quality 

