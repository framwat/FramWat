
### CALCULATION METHOD  
$DrainageD\: [km/km^2];$  

### DESCRIPTION  
Drainage Density - river network density. 

### GROUP  
general, drought, flood and quality  

