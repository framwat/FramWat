
### CALCULATION METHOD  
$PrecFreqLow50 [-];$  

### DESCRIPTION  
Frequency of precipitation lower than 50% of the multiannual average (in the growing season)

### GROUP  
general and drought  

