
### CALCULATION METHOD  
$WetlandRatio= \frac{wetlans\: area\: }{SPU\: area},  [-];$  

### DESCRIPTION   
Wetland area to SPU area ratio.

### GROUP   
general, drought, flood and quality  

