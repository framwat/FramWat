
### CALCULATION METHOD  
$ReclaimedRatio= \frac{reclaimed\: meadows\: and\: pastures \: area\: }{SPU\: area},  [-];$  

### DESCRIPTION   
Reclaimed meadows and pastures area to SPU area ratio; Intersect meadows and pastures (CLC code: 231) with ditches (buffer 100m).

### GROUP  
general, drought, flood and quality  

