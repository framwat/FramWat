
### CALCULATION METHOD  
$EcoAreaRatio= \frac{Semi-natural\: land\: cover\: area\: }{SPU\: area},  [-];$  

### DESCRIPTION   
Semi-natural land cover types area to SPU area ratio.

### GROUP  
general and quality  

