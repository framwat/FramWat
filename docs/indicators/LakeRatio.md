
### CALCULATION METHOD  
$LakeRatio= \frac{lakes\ and\: reservoirs\: area\: }{SPU\: area},  [-];$  

### DESCRIPTION   
Lakes and reservoirs area to SPU area ratio.

### GROUP  
general, drought, flood and quality   

