
### CALCULATION METHOD   
$UrbanRatio= \frac{urban\: area\: }{SPU\: area},  [-];$  

### DESCRIPTION    
Urban area to SPU area ratio.

### GROUP   
general, drought, flood and quality  

