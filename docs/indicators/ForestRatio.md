
### CALCULATION METHOD  
$ForestRatio= \frac{forested\: area }{SPU\: area},  [-];$  

### DESCRIPTION   
Forested area to SPU area ratio.

### GROUP  
general, drought, flood and quality  

