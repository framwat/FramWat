
### CALCULATION METHOD  
$Pre\_Var\_m = \frac{pMin}{P}, i=1,NYears [-];$

### DESCRIPTION  
Precipitation variability in the multiannual period - ratio of the lowest yearly sum of precipitation (for the growing season) and multiannual P average.

### GROUP  
general and drought

