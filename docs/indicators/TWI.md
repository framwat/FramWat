### CALCULATION METHOD   
twi according to [gis4geomorphology](http://gis4geomorphology.com/topographic-index-model/) 

### DESCRIPTION  
Topographic Wetness Index - a steady state wetness index based on DEM 

### GROUP 
general, drought, flood and quality  

