### CALCULATION METHOD  
$FlowMaxAvgRatio =  \frac {swMHQ}{ swMMQ},[-];$  
where:  
swMHQ - high flow   
swMMQ - mean flow 

### DESCRIPTION  
Ratio of high flow (swMHQ) to mean flow (swMMQ), [-]; high flows assessment.  

### GROUP 
general and flood  

