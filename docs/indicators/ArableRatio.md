
### CALCULATION METHOD  
$ArableRatio= \frac{arable\: area\: }{SPU\: area},  [-];$  

### DESCRIPTION   
Arable area to SPU area ratio.

### GROUP  
general, drought, flood and quality  

