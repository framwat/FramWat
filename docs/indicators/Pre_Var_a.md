
### CALCULATION METHOD  
$Pre\_Var\_a = \frac {1}{NYears}*sum(\frac{pMax_i - pMin_i}{pAvg_i}), i=1,NYears [-];$

### DESCRIPTION  
Precipitation sum - average intra year variability- amplitude of monthly sum of (pMax~i~ - pMin~i~) divided by mean monthly precipitation, averaged for the multiannual period.

### GROUP  
general and drought

