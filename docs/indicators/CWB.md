### Calculation method
$cwb = \frac{1}{NYears} * sum(Pi-ETpi)$[mm]  
$i=1, NYears$

### Description
Accumulated for the growing season, averaged for the multiannual period.

### Group
general (drought)
