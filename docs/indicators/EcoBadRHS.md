
### CALCULATION METHOD  
$EcoBadRHS=\frac{total\: lenght\:of\:poor\: or\:bad\: morphological\:elements\: }{total\: lenght\: of\: water\: courses\: in \:SPU\:},  [-];$  

### DESCRIPTION   
Total length with poor or bad morphological  elements assessment and the total length of water courses in SPU.  

### GROUP  
general and quality  

