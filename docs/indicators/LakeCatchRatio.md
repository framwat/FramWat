### CALCULATION METHOD  
$LakeCatchRatio= \frac{Lake\: catchment\: area }{SPU\: area},  [-];$  

### DESCRIPTION   
Lake catchment area to SPU area ratio - Immediate lake's catchment area in SPU ratio.

### GROUP  
general and flood  

