
### CALCULATION METHOD  
$swr [mm]$  

### DESCRIPTION  
Maximum soil water retention based on the type of soil (general)

### GROUP  
general and drought  

