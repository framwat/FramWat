
### CALCULATION METHOD  
$EcoNumRatio= \frac{nr\: of\:semi-natural\: land\: cover\: patches\: }{total\: nr\: of\: land\: cover\: patches\: in \:SPU\:},  [-];$  

### DESCRIPTION   
Number of semi-natural land cover patches to total number of land cover patches in SPU.

### GROUP  
general and quality  

