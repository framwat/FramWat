
### CALCULATION METHOD  
$EcoCombined, \: [-];$  

### DESCRIPTION   
Combination of number of semi-natural land cover patches and their area (combination use EcoAreaRatio and EcoNumRatio and look-up tabele).

### GROUP  
general and quality  

