
### CALCULATION METHOD  
$sri =  \frac{swMMQ}{P} [-]$  

### DESCRIPTION  
Surface Runoff Index - ratio of multiannual mean flow (MMQ=Q~50%~, expressed in mm) and average sum of precipitation [mm]; caculated for internal runoff from sub-basins, without imported (flowing from upstream) water.

### GROUP  
general, drought and flood  

