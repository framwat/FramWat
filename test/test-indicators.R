context("indicators")

#library(RSelenium)
library(testthat)
library(rgdal)
library(raster)
library(logging)
library(rgeos)
library(dplyr)
library(sp)
library(data.table)
library(dynatopmodel)
library(snow)
library(smoothr)

summary <- function(x) list(
    min = round(min(x), 2),
    max = round(max(x), 2),
    mean = round(mean(x), 2),
    stdev = round(sd(x), 2)
)

source("../R/server/indicators/density.R")
source("../R/server/indicators/drainage.R")
source("../R/server/indicators/ecoAraBuf20mRatio.R")
source("../R/server/indicators/ecoBadRHS.R")
source("../R/server/indicators/ecoCombined.R")
source("../R/server/indicators/ecoNumRatio.R")
source("../R/server/indicators/meanderRatio.R")
source("../R/server/indicators/nonForested.R")
source("../R/server/indicators/onePointToSPU.R")
source("../R/server/indicators/onePolyToSPU.R")
source("../R/server/indicators/oneRasterToSPU.R")
source("../R/server/indicators/preVarM.R")
source("../R/server/indicators/rainfallErodibility.R")
source("../R/server/indicators/ratio.R")
source("../R/server/indicators/reclaimedRatio.R")
source("../R/server/indicators/slopeLand.R")
source("../R/server/indicators/slopeRiver.R")
source("../R/server/indicators/soilErodibility.R")
source("../R/server/indicators/sri.R")
source("../R/server/indicators/twi.R")
source("../R/utils.R")

spuMap = readOGR(dsn="../test/input/spu")

spuMap$AreaSPU <- abs(area(spuMap))
spuMap$ID <- (1:length(spuMap))

arableMap = readOGR(dsn="../test/input/arable")
arableMap = raster::intersect(arableMap, spuMap)
arableMap$area <- abs(area(arableMap))

bfiMap = readOGR(dsn="../test/input/bfi")
bfiMap = raster::intersect(bfiMap, spuMap)
bfiMap$area <- abs(area(bfiMap))

#this is map of lines and there is no way to calculate area
ditchesMap = readOGR(dsn="../test/input/ditches")
ditchesMap = raster::intersect(ditchesMap, spuMap)

floodExtendMap = readOGR(dsn="../test/input/floodextent")
floodExtendMap = raster::intersect(floodExtendMap, spuMap)
floodExtendMap$area <- abs(area(floodExtendMap))

forestMap = readOGR(dsn="../test/input/forest")
forestMap = raster::intersect(forestMap, spuMap)
forestMap$area <- abs(area(forestMap))

lakeCatchMap = readOGR(dsn="../test/input/lakecatch")
lakeCatchMap = raster::intersect(lakeCatchMap, spuMap)
lakeCatchMap$area <- abs(area(lakeCatchMap))

lakeMap = readOGR(dsn="../test/input/lake")
lakeMap = raster::intersect(lakeMap, spuMap)
lakeMap$area <- abs(area(lakeMap))

landUseMap = readOGR(dsn="../test/input/landuse")
landUseMap = raster::intersect(landUseMap, spuMap)
landUseMap$area <- abs(area(landUseMap))

meadPastureMap = readOGR(dsn="../test/input/meadpasture")
meadPastureMap = raster::intersect(meadPastureMap, spuMap)
meadPastureMap$area <- abs(area(meadPastureMap))

nonForestMap = readOGR(dsn="../test/input/nonforest")
nonForestMap = raster::intersect(nonForestMap, spuMap)
nonForestMap$area <- abs(area(nonForestMap))

#this is map of points and must not be intersected
precVegMap = readOGR(dsn="../test/input/precveg")

precAnnMap = readOGR(dsn="../test/input/precann")

#this is map of lines and there is no way to calculate area
rhsMap = readOGR(dsn="../test/input/rhs")
rhsMap = raster::intersect(rhsMap, spuMap)

#this is map of lines and there is no way to calculate area
riverMap = readOGR(dsn="../test/input/river")
riverMap = raster::intersect(riverMap, spuMap)

seminaturalMap = readOGR(dsn="../test/input/seminatural")
seminaturalMap = raster::intersect(seminaturalMap, spuMap)
seminaturalMap$area <- abs(area(seminaturalMap))

swFlowMap = readOGR(dsn="../test/input/swflow")
swFlowMap = raster::intersect(swFlowMap, spuMap)
swFlowMap$area <- abs(area(swFlowMap))

orchardMap = readOGR(dsn="../test/input/orchard")
orchardMap = raster::intersect(orchardMap, spuMap)
orchardMap$area <- abs(area(orchardMap))

urbanMap = readOGR(dsn="../test/input/urban")
urbanMap = raster::intersect(urbanMap, spuMap)
urbanMap$area <- abs(area(urbanMap))

wetlandMap = readOGR(dsn="../test/input/wetland")
wetlandMap = raster::intersect(wetlandMap, spuMap)
wetlandMap$area <- abs(area(wetlandMap))

cwbMap = raster("../test/input/cwb/cwb_sub200m.tif")

demMap = raster("../test/input/dem/DEM.tif")

dem200Map = raster("../test/input/dem/DEM200.tif")

grrMap = raster("../test/input/grr/grr.tif")

swrMap = raster("../test/input/swr/swr.tif")

soilSandMap = raster("../test/input/soil/SoilSand_Fraction.tif")
soilSiltMap = raster("../test/input/soil/SoilSilt_Fraction.tif")
soilClayMap = raster("../test/input/soil/SoilClay_Fraction.tif")
soilOrgCMap = raster("../test/input/soil/SoilOrgC_Fraction.tif")

test_that("Indicator.ArableRatio", {
    result = densityCalculation(spuMap, arableMap)
    result = summary(result)
    expect_equal(result, list("min" = 0, "max" = 86.95, "mean" = 33.49, "stdev" = 30.1))
})

test_that("Indicator.BFI", {
    result = onePolyToSPUCalculation(spuMap, bfiMap, "bfiMap$bfi", bfiMap@data$bfi)
    result = summary(result[["values"]])
    expect_equal(result, list("min" = 1.09, "max" = 16.48, "mean" = 4.72, "stdev" = 4.91))
})

test_that("Indicator.CWB", {
    result = oneRasterToSPUCalculation(spuMap, cwbMap)
    result = summary(result)
    expect_equal(result, list("min" = -263.01, "max" = -83.57, "mean" = -165.22, "stdev" = 47.31))
})

test_that("Indicator.DrainageD", {
    result = drainageCalculation(spuMap, riverMap)
    result = summary(result[["values"]])
    expect_equal(result, list("min" = 0.04, "max" = 4.24, "mean" = 0.69, "stdev" = 0.54))
})

test_that("Indicator.EcoAraBuf20mRatio", {
    result = ecoAraBuf20mRatioCalculation(spuMap, riverMap, lakeMap, arableMap)
    result = summary(result[["values"]])
    expect_equal(result, list("min" = 0, "max" = 3.02, "mean" = 0.29, "stdev" = 0.53))
})

test_that("Indicator.EcoAreaRatio", {
    result = densityCalculation(spuMap, seminaturalMap)
    result = summary(result);
    expect_equal(result, list("min" = 0, "max" = 100, "mean" = 43.28, "stdev" = 33.5))
})

test_that("Indicator.EcoBadRHS", {
    result = ecoBadRHSCalculation(spuMap, riverMap, rhsMap)
    result = summary(result[["values"]])

    expect_equal(result, list("min" = 0, "max" = 100, "mean" = 24.9, "stdev" = 33.06))
})

test_that("Indicator.EcoCombined", {
    result = ecoCombinedCalculation(spuMap, landUseMap, seminaturalMap)
    result = summary(result[["values"]])

    expect_equal(result, list("min" = 1, "max" = 5, "mean" = 2.95, "stdev" = 1.45))
})

test_that("Indicator.EcoNumRatio", {
    result = ecoNumRatioCalculation(spuMap, landUseMap, seminaturalMap)
    result = summary(result[["values"]])

    expect_equal(result, list("min" = 0, "max" = 100, "mean" = 37.19, "stdev" = 23.81))
})

test_that("Indicator.FloodRiskAreaRatio", {
    result = densityCalculation(spuMap, floodExtendMap)
    result = summary(result)

    expect_equal(result, list("min" = 0, "max" = 90.52, "mean" = 2.93, "stdev" = 10.12))
})

test_that("Indicator.FlowMaxAvgRatio", {
    result = ratioCalculation(spuMap, swFlowMap, "swFlowMap$swMHQ", swFlowMap@data$swMHQ, "swFlowMap$swMMQ", swFlowMap@data$swMMQ)
    result = summary(result)

    expect_equal(result, list("min" = 0.01, "max" = 17.72, "mean" = 6.9, "stdev" = 6.51))
})

test_that("Indicator.FlowMinAvgRatio", {
    result = ratioCalculation(spuMap, swFlowMap, "swFlowMap$swMLQ", swFlowMap@data$swMLQ, "swFlowMap$swMMQ", swFlowMap@data$swMMQ)
    result = summary(result)

    expect_equal(result, list("min" = 0.18, "max" = 16.43, "mean" = 1.08, "stdev" = 3.25))
})

test_that("Indicator.FlowMinMaxRatio", {
    result = ratioCalculation(spuMap, swFlowMap, "swFlowMap$swMLQ", swFlowMap@data$swMLQ, "swFlowMap$swMHQ", swFlowMap@data$swMHQ)
    result = summary(result)

    expect_equal(result, list("min" = 0.01, "max" = 149.36, "mean" = 28.65, "stdev" = 40.41))
})

test_that("Indicator.FlowVarRatio_m", {
    result = ratioCalculation(spuMap, swFlowMap, "swFlowMap$swLMQ", swFlowMap@data$swLMQ, "swFlowMap$swLMQ", swFlowMap@data$swHMQ)
    result = summary(result)

    expect_equal(result, list("min" = 0.21, "max" = 0.75, "mean" = 0.43, "stdev" = 0.23))
})

test_that("Indicator.ForestRatio", {
    result = densityCalculation(spuMap, forestMap)
    result = summary(result);

    expect_equal(result, list("min" = 0, "max" = 100, "mean" = 34.52, "stdev" = 33.08))
})


test_that("Indicator.grr", {
    result = oneRasterToSPUCalculation(spuMap, grrMap)
    result = summary(result);

    expect_equal(result, list("min" = 66.95, "max" = 105.04, "mean" = 84.89, "stdev" = 14.13))
})

test_that("Indicator.LakeRatio", {
    result = densityCalculation(spuMap, lakeMap)
    result = summary(result)

    expect_equal(result, list("min" = 0, "max" = 11.2, "mean" = 0.18, "stdev" = 1.18))
})

test_that("Indicator.LakeCatchRatio", {
    result = densityCalculation(spuMap, lakeCatchMap)
    result = summary(result)

    expect_equal(result, list("min" = 0, "max" = 100, "mean" = 62.95, "stdev" = 47.45))
})

test_that("Indicator.MeanderRatio", {
    result = meanderRatioCalculation(spuMap, riverMap)
    result = summary(result[["values"]])

    expect_equal(result, list("min" = 59.64, "max" = 99.75, "mean" = 85.42, "stdev" = 8.45), tolerance = 0.1)
})

test_that("Indicator.NonForestedRatio", {
    result = nonForestedCalculation(spuMap, nonForestMap, demMap)
    result = summary(result[["values"]])

    expect_equal(result, list("min" = 0, "max" = 59.39, "mean" = 8.88, "stdev" = 13.45))
})

test_that("Indicator.OrchVegRatio", {
    result = densityCalculation(spuMap, orchardMap)
    result = summary(result)

    expect_equal(result, list("min" = 0, "max" = 8.3, "mean" = 0.1, "stdev" = 0.8))
})

test_that("Indicator.Pre_Var_a", {
    result = onePointToSPUCalculation(spuMap, precVegMap, "precVegMap$preVar_a", precVegMap@data$preVar_a)
    result = summary(result[["values"]])

    expect_equal(result, list("min" = 1.65, "max" = 1.96, "mean" = 1.84, "stdev" = 0.06))
})

test_that("Indicator.Pre_Var_m", {
    result = preVarMCalculation(spuMap, precVegMap)
    result = summary(result[["values"]])

    expect_equal(result, list("min" = 0.55, "max" = 0.75, "mean" = 0.64, "stdev" = 0.05))
})

test_that("Indicator.PrecFreqLow50", {
    result = onePointToSPUCalculation(spuMap, precVegMap, "precVegMap$pFreqLow75", precVegMap@data$pFreqLow75)
    result = summary(result[["values"]])

    expect_equal(result, list("min" = 1, "max" = 6, "mean" = 2.77, "stdev" = 1.37))
})

test_that("Indicator.RainFallErodibility", {
    result = rainfallErodibilityCalculation(spuMap, precAnnMap, precVegMap)
    result = summary(result[["values"]])

    expect_equal(result, list("min" = 591.26, "max" = 1488.14, "mean" = 749.36, "stdev" = 214.84))
})

test_that("Indicator.ReclaimedRatio", {
    result = reclaimedRatioCalculation(spuMap, ditchesMap, meadPastureMap)
    result = summary(result[["values"]])

    expect_equal(result, list("min" = 0, "max" = 29.69, "mean" = 2.39, "stdev" = 4.38))
})

test_that("Indicator.SlopeLand", {
    result = slopeLandCalculation(spuMap, demMap)
    result = summary(result)

    expect_equal(result, list("min" = 0.18, "max" = 4.46, "mean" = 1.69, "stdev" = 0.81))
})

test_that("Indicator.SlopeRiver", {
    result = slopeRiverCalculation(spuMap, demMap, riverMap)
    result = summary(result[["values"]])

    expect_equal(result, list("min" = 0.1, "max" = 3.6, "mean" = 0.72, "stdev" = 0.63))
})

test_that("Indicator.SoilErodibility", {
    result = soilErodibilityCalculation(spuMap, soilSandMap, soilSiltMap, soilClayMap, soilOrgCMap)
    result = summary(result[["values"]])

    expect_equal(result, list("min" = 0.08, "max" = 0.18, "mean" = 0.13, "stdev" = 0.04))
})

test_that("Indicator.SRI", {
    result = sriCalculation(spuMap, precAnnMap, swFlowMap)
    result = summary(result[["values"]])

    expect_equal(result, list("min" = 0.02, "max" = 0.35, "mean" = 0.23, "stdev" = 0.07))
})

test_that("Indicator.swr", {
    result = oneRasterToSPUCalculation(spuMap, swrMap)
    result = summary(result)

    expect_equal(result, list("min" = 107, "max" = 525.4, "mean" = 431.13, "stdev" = 61.47))
})

test_that("Indicator.TWI", {
    result = twiCalculation(spuMap, demMap)
    result = summary(result[["values"]])

    expect_equal(result, list("min" = 9.71, "max" = 20.2, "mean" = 11.26, "stdev" = 1.38))
})

test_that("Indicator.UrbanRatio", {
    result = densityCalculation(spuMap, urbanMap)
    result = summary(result)

    expect_equal(result, list("min" = 0, "max" = 89.63, "mean" = 9.07, "stdev" = 14.2))
})

test_that("Indicator.WaterYieldAvgFlow", {
    result = onePolyToSPUCalculation(spuMap, swFlowMap, "swFlowMap$swMMQ", swFlowMap@data$swMMQ)
    result = summary(result[["values"]])

    expect_equal(result, list("min" = 8.97, "max" = 201, "mean" = 142.82, "stdev" = 45.94))
})

test_that("Indicator.WaterYieldMinFlow", {
    result = onePolyToSPUCalculation(spuMap, swFlowMap, "swFlowMap$swLMQ", swFlowMap@data$swLMQ)
    result = summary(result[["values"]])

    expect_equal(result, list("min" = 62.71, "max" = 119, "mean" = 86.46, "stdev" = 16.57))
})

test_that("Indicator.WetlandRatio", {
    result = densityCalculation(spuMap, wetlandMap)
    result = summary(result)

    expect_equal(result, list("min" = 0, "max" = 39.57, "mean" = 4.15, "stdev" = 6.42))
})