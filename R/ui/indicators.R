
getInputsLabels <- function(inputs) {
    inputsList <- list()

    for (input in inputs) {
        if (input$name != "Subbasin area") {
            inputsList <- listAppend(inputsList, tags$a(paste0(input$name, ";"), class="data-input"))
        }
    }

    return(inputsList)
}

getDescriptionForNecessityType <- function(type) {
    if (type == "N") {
        return("default stimulation direction of indicator shows need for water retention")
    } else if (type == "P") {
        return("default stimulation direction of indicator shows possibility for water retention")
    }

    return("")
}

getIndicatorRows <- function(indicatorId, indicator, inputs, status, isRecommended) {

    indicatorTitle <- list(indicatorId)

    if (indicator$necessity == "NP") {
        indicatorTitle[[2]] <- list(
            tags$span("N", class="label label-necessity-n label-help", title=getDescriptionForNecessityType("N")),
            tags$span("P", class="label label-necessity-p label-help", title=getDescriptionForNecessityType("P"))
        )
    } else if (indicator$necessity != "") {
        indicatorTitle[[2]] <- tags$span(
            indicator$necessity,
            class=paste0("label label-help label-necessity-", tolower(indicator$necessity)),
            title=getDescriptionForNecessityType(indicator$necessity)
        )
    }

    if (isRecommended == TRUE) {
        indicatorTitle[[3]] <- tags$span("Recommended", class="label label-primary label-help", title="usually good quality data available and easy to calculate")
    }

    return(
        list(
            tags$tr(
                tags$td(
                    checkboxInput(
                        paste0("indicator", indicatorId),
                        NULL,
                        FALSE
                    ),
                    class = "cbox"
                ),
                tags$td(
                    list(
                        tags$div(indicatorTitle, class="indicator-name"),
                        tags$div(indicator$name)
                    )
                ),
                tags$td(indicator$topic),
                tags$td(getInputsLabels(inputs)),
                tags$td(
                    tags$span(
                        "Not set",
                        class=paste0("label"),
                        id = paste0("indicatorStatus", indicatorId)
                    )
                ),
                tags$td(
                    actionLink(
                        paste0("toggleDescriptionIndicator", indicatorId),
                        'show description',
                        class = "show-more"
                    )
                )
            ),
            tags$tr(
                tags$td(
                    tags$div(
                        withMathJax(includeMarkdown(paste("docs/indicators/", indicatorId, ".md", sep = ""))),
                        class = "description"
                    ),
                    colspan = 5
                ),
                id = paste0("descriptionIndicator", indicatorId),
                style = "display: none"
            )
        )
    )
}

getIndicatorsTable <- function(indicators, inputs, statuses, recommended) {
    rows <- list()

    logdebug("getIndicatorsTable")

    for (indicatorId in attributes(indicators)$names) {
        indicator = indicators[[indicatorId]]

        indicatorInputs = list()

        i = 1

        for (input in indicator$input) {
            input = strsplit(input, "$", fixed = TRUE)[[1]][[1]]

            indicatorInputs[[i]] = inputs[[input]]
            i = i + 1
        }

        if (is.null(statuses[[indicatorId]]) == FALSE) {
            status = statuses[[indicatorId]]
        } else {
            status = NULL
        }

        rows[[length(rows) + 1]] <- getIndicatorRows(
            indicatorId,
            indicator,
            indicatorInputs,
            status,
            is.element(indicatorId, recommended)
        )
    }

    return(
        with(
            htmltools::tags,
            table(
                thead(
                    tr(
                        th(""),
                        th('Indicator name'),
                        th('Topics'),
                        th('Required input data'),
                        th('Status'),
                        th('')
                    )
                ),
                tbody(
                    rows
                ),
                width = "100%",
                class = "standard-table padded"
            )
        )
    )
}