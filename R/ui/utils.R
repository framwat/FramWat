currentNavBar = 1

setActiveNavBar <- function(n, force) {

    if (force == FALSE && n <= currentNavBar) {
        return()
    }

    currentNavBar = n

    for (i in 1:3) {
        shinyjs::removeClass(id = paste0("nav-progress-", i), class = "active")
    }

    shinyjs::addClass(id = paste0("nav-progress-", n), class = "active")
}