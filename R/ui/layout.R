getLayout <- function(version, isDebug) {

    if (isDebug) {
        version = paste0(version, "-debug")
    }

    getOutputConsole <- function() {

        if (isDebug) {
            return(
                tags$div(
                    tags$div("LOGS:"),
                    tags$pre(
                        textOutput("messageLog")
                    ),
                    actionButton("refreshLog", "Refresh log"),
                    id="logWindow",
                    `style`="display:none"
                )
            )
        }

        return(c())
    }

    getOutputConsoleButton <- function() {
        if (isDebug) {
            return(tags$li(
                tags$a(href="#", "LOGS", `onclick`='$("#logWindow").toggle()')
            ))
        }

        return(c())
    }

    return(
        list(
            fixedPanel(
                top = 0,
                left = 0,
                right = 0,
                height = 52,
                class = "top-navbar",
                tags$div(
                    tags$img(src = "data:image/gif;base64,R0lGODlhWwGWAPcAADhzXxtbeiBbeihmbTFrajpzYidkczprdkN7WU55bVB5bUFtdGiaOXSfMm+hLnimK3OhMk2ET1aKSV+SQU2DUVCCUmGSQW2NXn2YV12DZmuMYZm/D423Gpm/EIavIYaxIqy3PpnAD5nAEaHDH5/DIaPGJ6vLO7vBOvnqHdrVLMrLM+nfJenhI/rqIIGXV4uiT5ysSYyiUau2Qa3MQbTPTbLQTLXPULjTWb7XasDXa8bceQFFjhFOig9QihdWggBFkhFPmQ5SmBRTmSFZgyFcnR5coiNdoS1npjFopz50rT50sUB0rVV3rkR2sVF9tE6AtFuIuGGLunGNvm+Wv3SWvm6OwW6YwnSWwn+iyMzeiMzgiNLjltnnpd7stOHsuOXwvoaaxYOkyZGszo+t0ZOr0Z651qOs06a217S627a84K7F3LfD3r7Q363H4b3M47/Q48LM3sDQ3+XvxOXww+zz0/L338LN48jW59LY6tve8N3l7t7n8eLo7/T55uXp8/Dv9e7z+P///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C05FVFNDQVBFMi4wAwEAAAAh/iBSZXNpemVkIHdpdGggZXpnaWYuY29tIEdJRiBtYWtlcgAh+QQAFAD/ACwAAAAAWwGWAAAI/wADCRxIsKDBgwgTKlzIsKHDhxAjSpxIsaJFi3fAaNzIsaPHjyBDihzJEY/ANWZSqlS55qLLlzBjypxJs+bEMzt+5PzBs6fPn0B57gxKtKhRoGhOrlza0qbEP4D+QAVElarTq1izaoV55qjXr2DD/kwaCOVSllsZ/jlzVqWftHDjyt3aVazdu3fJmm3bdG7BtW1T/vFLuLBhiXXxgh2q+KjewGb6GgYcePDhy5gzB0rcuCjjzkYfB5ZcmHJby5pTq07LGbRrvKL5YjZ9FvXq27hntn7tU0EG3kRjnyVNmPZS27mTK78J/OcBFypUaFjQ3GdSQHuHA7psfCXy5eDDJ//crbiHC588YKBAAYOxCx68hTOdzbay+Pv4C5K3y0OBixQKHNCTAisA2NMBCqjgggI9uEYWGpAR5xAgfOSBxx158PEdRN2ptKFEgPihBx4X5pHhdllRWKJLfuRxBx4oTiiihS/m4UeML/2Rhx1roOHjGnjwgWMgepho5JFDIrSfXTGwgIIKQ/SUwRBD/MbTECqgwEIMr8m3koQKAYIHhG2hAeNDefjoI2RmnKHmm3ZMmMca9ZV55kN+vKknGjj6kR0cBrmxp5p5EATIHXWekeRBgMzJppmLQsRHdmedcUeMZLbFR0NLitVDgTB4FUMKK3ymmJdoTYgHmyvB8WFBq7L/+ihDgNhRp6x2RGqQH2zGaMdZgBaU6VkmCZTHrW3qKtChyLKZ60R/DMvmGYUGIu1KmzLUaVhDaNCDBl5pEMSUXSo1mkN8NDttsQrFKmtgZCWkh7qsUtsQr5ChSGlKwRJ0rUrFuruSogrx8W+97Dp07LsrXXqwGdkutK1RDb4W5anmysaQwAzzuxDHHacU70F3hHxWnAvhG9h2v7bV70APm8RHYGe8CnLIKDeUh8kqwfFwxApNTNQOIMDnmgyNoZrSGsoGAgfPK/GZ0M0dj1xQyVCvlDNCM+erB2QvCxRzh8kiRHXITDP0ddasVisxXhrIsMIJMZi6WAwnoCDDeXll/zzcQi2zHdnUgls90M6Cq3SHQmsHZjDYBsUceKWLIp64GVszSu/lQI+HVw95pyBgY0Ok8GTFdik9OOGXY2524QeRDbUeCanMF5thWxvh5gT/tTnPtCc0OWRn/O44p3jtcMIJKoDGgwoynIBxWREm9EfrKrlN0NnvGk69rD4ab4bUB9lucu4P15vk8PCiIX7vBl3vrJAD/YGH+Cl1rmTyAg6BOl48iBJ1YOO3+SAkfW2yA4nsgED4DYR74DNI4+CVhxj54WlsWlz52Ia+rDkwEOY7C6QG4gf2LSVzAzGhyN7CKKyxSn8HEVp1mqM6MFkuMHc63O9QSBA/bI6FDNmX1v8WNS/iLSqEHesg1D6IwcDAYVHpMqJBAEEv8iXkhsfT1gy3WJQaHpBNwYvf73QlOxiKMTBk4OFA+AAGKbjRjVFwghwlpAc5OoEJTLjjHZkQhTe+UQwK1AMQB4K/L72oc1SEjBoF4kPIJEwgExzYq7b3QuTdZXTNwaRYvGiQrgVmkYHA4lIeSRD5BWaQCUEDE5awBCQYwQhEEIIQmDDJzQyNCUnCw1dyQoUcTpEJshQCEV6JhCXk0Ql9lIIZKnhFyFgRIaJUieFcuBQNNkSIKjGjfu7SAxXYrTPN69v3NFYQFaakaZvznkDkRwYxbMSNTgijQpjQRVrRMyhB0J8uj7L/AyLk7pf8/IEYlAWINkqBChoJAxlSQsopHqxmBRHiBxnHJu15Liw7+A8KUsCl14zqSXwLCyf140yGmNMMkdIDEmQZhCD8pKEF0QMRiEKEWgoEDEUhzj6NQgRrhumeRjEDQ/xghJ+0dJaofF1gtAeIf4EJIYnM4tvEwoO8ncBornkeCk7wzXqO828FAUQG1WYGMmiEClK4IxL8aTYhFAWm2ysKGByyU6CIwSB1JcpTwwpUolChaYHIq0+kgK6ELlRrhlIXXBHSxLNokyAy7EkP9sZV4JxAA9JLXQG/tCswbowILvVJTnagzjy4lSiLFch+dsAEhuGUKFIYkmB/EtsJ//UVKESw6AGL0tp3vTYIwjxCHpVpKKk6hJpLeSwhufmDHnTVLjto0P/AMtKBRFIlZt3DUE8bFKEeBA/cBUpqA/FaLiJBtkbZQRweAojb/mSuDREDF3mCy/ox4aAacUtEzqZc1c73vz2pLiTN4M42MmGtQgiCPBMyU6J41yCmfStDovDfIyBntj1Bgk0H0l6jjPem/yVCxPzgE+AagQm6BRxk+mtLAP9XwIG4A2hD+5OkHqTB3YVmeF/KECiE+MJGga9ti2IEGx+kvFsUcf12rJMzRASbELMk3F6zgww8NzR+Mytal0DYgsDBKA5xb08eXJAIo7bHP4aVZ5zMXjHrpP/LDUHyDJW8ziMAZQdU4NDmWBxZn/QASwuYrl2GcIAUUImAm2njEoyQ4J4wwSBfLkqYi0Lm7TG5J+OlAmjstpOaqrko/0xIh4kiZIbg9JtDuTJNIwYIKQTlCEY+CBpOLAWNHDZ/UhaLBky3gro1JgYr2Ci4xOljojzay2BuiJt/UOkHXpon45XvYoKC6h+c99NEYTGHl31Xh5hB1Y1BwiClDRQ2M4QPRe2Jie8LhhQbpM8+kQELvNmZHaiABZkV57KPTZBIE2XSDtaxhBfib6BEwQ9SSbjCF87whOfSKNpe1rKbnZCC/4SWDc94xod0B5q621CaNopPLwrdGCwABoL/9hQMFtBRffMW0slmyMQFfuaP0fgnQoh1RTDMk4gHYtQ5RtfNfULnmPzBzkFhwoLDKuegjHx/ydPJULB6FPiMlidU36RA9g1zSSub0jQPyniBDhSKX4TnP/A52X9idkbNfCbkBgoSQn2HZfdkB0+PIW+G0PL0ojw+W385sr0uc7AfxMxB2etAmt4TIiwdIXpQJ7aDova3OyTuP8ktup7JNRwDJQhMMJOJxsSEoQ8t7+92TQ9iIDcYWOnuPskADFIggxhkHdFcH/y/vx7wwz/7B6WuH6xM72idC+QOSBBo09BeecMrjPj07S8ekg8GwJphaD6RpWi9gvdcN8YFTlKB/yYVMOwrZYkFLgC3YwJv7K7vvvC9h/DvgwCxP7QIDlFotqtvCVc/iIG7YDBJzAcRa+cTbYcQIWdsqAdCYkAEO0EFtQQIxeYZ6idaCwhZr9EDKUB7PdEty6MBF/MDs5cCt4d7gtdvMbcQllcQfuB5QUEER3AEp9VseAB9PCEEVAAHepBwepARSDdYsTaAbeZ8dPV7P4CDOsiDdyAGyfcTUKBcKlWBJfZ7F7hcrjEEDPJ6PaAe7IE6GdADChCCnUEWuYeChKeCRFgQTRgWFMd4uIUEcOiCF/d4gQVxBLiC3vYVRACHa2UUTJBaeYAEUvgDRKBKRVGF/sVFBJICrzdDZP/hBCc4EBb3EwAXdAdBYWJBcX/ABINoFEuwIUI4ZPHHXhM4bUMjbgvBB1BQgf4ECEtwiN7XHBlwAAgyX2QYiQIxiT5RiWWXEGtgF2anUsBhbpMHFM03ig/BB3ZnF922EFT0g0YhBbTjirCoRS5mi+yXdO4XFLzIdrUjh0ElL2vYGcRYjD9xjJYYEXqwjF8RBAP1ENECBUKwA/RIdGAQByhCjdSGiC12jVv0iLjoNJ7RjQYYNJlYMFLQiT0hBJJXh0WBjr1IEX6wf4ohBOU4IfaTKUCCHHxggx8Gb/4oUgIBie2ne9zIe+l4ZGzojGYAjun1A1CQWqHYEAU4ZhbRVC7/mV5/iBWYR3R0uE0h6YgjGZC62BMEaZNh0pJeIQUfxgcNGBZMkDYfY4dDiIwT4X/p9hWhB1iMRBFwYIP8NlVBSUNb901hmYuxJExEsJZp6RBOwJZwKUsXeRAzA0ypNkti4EtqsQZgYGf0yBigJwaXUoSyBJdsaXx8pZaGSQRzCS1w0JdEAXpg8Cy21UsRgQY5KVDHJQac2Zme+ZmgGZqiOZqkWZqeWSwPowaxo3EYuXHsZX93EJsIx5Wixgdx4D5tspG0WUqsGREaBxUzESIMxBZosAZ3cCMSEWlIiJh5AAY2eIQflx/SOZ3UGZwUyRNHoEwYIkhRwQd4sAZSkJk6/xF81Vme5nmeEiFYQxEEe3hgoAUWRvCT6Dmf9DmfbtgZQaB49bmf/HkfemCEitGM/TmgBHofPQka9FegCrqgydFqzWEEocagEjqhftFU7OgVQgAGPkehHNqhNCEmZgAFMyWFRyAG0emhKJqiNhEVY+JOUeCewaSWTEAFgrlhBPcjuwkTNiKJ8dOQKvqjhEEV9ucHREqkwAkT7RUGbmKjMYEG/YIEYgSkUjqh7eU2OigQdwUHfPlEZkCjgyEGZ0Cj26EHGrE4fxAGYPCOnEkFKAUzT+p/YsCmgXA9cxqnKBV5YUAFjTmlfIof7SUGa8A0ZrA47RUIZoBi1yMGefJXRv+ABn4ABij1h+t4I3jwqIASBHAwkdbkpAOBBHoQBMfJBHewB1AQCFTgqGYABncgBEHiqX36qtOZpG4iVoT6aGYAKO2lXYAgbteGB39IBJyJBJcCB2IABYACpdbSL5wqEJ6KrE6qioEQBMtyBHcQBTfFj7CarZpRpQMxqD9nq7W6KbvKBxoWWH/IBHyQroDApnogBoCyBGKjrMGyq3pQqslKqtE6rRlxrdrar8vRXmugB3qQSH4Cpd4aCGDqJ7FlBGsAp+11nAO7BHegiu8arwNRd3owM2BQr/G6B48mBQ17BqoKX6pKl4gCpld6EIG6sizLsutFQoEqn04TsIzUsjb/y7KWcQc3uwZwkAc2CghaWoV/EKg5mkI7y7JP9EA864wxayhHGyRcCQh68DRiYCY611Q7GySMUpxHG6gfhrAbQaRgQKNClbJ/EKcauqtdGoCBNbbVBwcIdQYm0W1wkDBrQAVsGiLe9SK8Mqdu+weRJzYLdgdU0GB/KQRSkLQEAaA/EJZ2oBP1xWCkdXzbB3s+USz3aW2KyhB34FJEoHO6JARMShD711WwtniNqywd+QOSwQe/RwSJqxA+VHo/cQRgAFN8cHWWSwSWWUpfQZ7sJWoS93NhxWETErXO6Jz0SAQzCgU/GAVA41ZIgEfUW71MUGr+BrwD4YBk4avW61bM/2u9waNp4Vu9SBBaSJBapbundehpEuFq5Wu9ePRXqBu5XONWretW8ssEI+qOjAVU04tHbrUDQtCmLJgT8nu+PEEEkvEH9LW/1HuALlGombGJ9NWw2xEid+BqrLW4P8AnVRHCVWGGPPFUMxUvItxhAyXCA6FpsSXCIvJ/rGUz+jvDU0OIozsQrlZ9KRzCBAEGrKW6+EsQrvsDMALDa8CJHxw5bhUEtzsVFGIGgvgD9NtDPwCqIvwHM2MEBMwuf5ATT9TDI3wViJkWgICJ7zhFa9BTHqyfkniDhEiHJ5wQ9NR2LlxxQbADe4pTTGAHQ2w2ODwRO/wQQGy/dPnHAlHEdP/oB/RES/0mvajHB1SQE8FHYkEAU3hwBHhWPzrxtQuhB4EKSi+BHUBCcA+RqS0moOVTEC7lxrnIE3VhyANhZ5JXxwmBU3B2EJomy4k8Uy3harwsELrkvhExyA7Bx0LMukTsVj+5T+zCyDuABNG5rlf8MpYMV11RZOvUyS/xrG+Rjz9nFVHBYUA0zsvSQzGyM3oQBhqEnCA0FcVypHOKGiiCBkIyU8GsEK18yjxRUDyhysy6xAhhywj4A7kMc0SwKNdXrl/5A6g3zDksEMYcZ6lbMIgcCIpsPcnXF12BqUPVyBZ0xXClB0cYz9zsEmhwKVJ7HXzCmSTCmUkRBpzpZHj/wJmLwxZZKgapiiJTG1hJwZlhALRl4E5iZS0u/aipahJsMVf2HGmePBAXTXA6QRX3lDu0TMfMlhB3nBAdl9DxI4je1WF59l2B/L7AR8gVnRBFnL8/8JMd1hQdFgVciQYiTUJ1jRAOfMmMBG0vIbK3uio/h1L2Wn0IqwdyTV6tZiHWKragDCgpDUlsCgZ+sIROCQeEdbaIPVdnzEZEIlRimyZsRIgTEdUKEWnbkQczVXQCMccDndUIgcsKgQY7YARJ8os5BzNX/HihG9GBkJDaq5L5nMgXndF4nXzBsrqu7Adu9WDXfEXQuc1G3M3VAtjXU1ACMdYpPdZmoAdLUDxq/+B/YfqdJrG0nY0Hc8WXxRN5Lk1ee8BmULAHLcFGZxumd2DPrzjWEaG/IRE2X7YD+gK5OHLVrW3HBq0QFHbQ33rW9SOIAF2HoivI1hYSVoPMFq3Ma8TMXH3F1dJxCmZbm7zXeo0Q31auc8oTURASMusvdoBwbAS0KAVfYrCDkg0FghRbVDCwQ4sGf3C7pUzePW0pNR0VQJvS21F9UqDFVADfGA0GcKAoqmrPRfXbCmGEeoxs/o26mjnLAn0QBK3LO4DgMLPPyBbi3XqESYUHBMzb1/kT9Bh8hZzMbE2H65jWcNBP2nbGVGzXZH41MyVkDkyB2BpjnKmaxBknTSE/i/8jBWcgmAx4q2c7Pt65KXgQPH5gElT0PUhdBn/VEjUtBlJhEkMLCDq9BnuAB3+QfPgNEfoNEvzdz/bFE/3C2lzu2ioJvUVapObtUrx84EPCBw5Ig2kO4Ugg4QVB4Wo93G6VqbcuImeQfB79ykKQ4sviY/htycp+6+06U0cQRg68AycOEtJOEVcwE3/wVztetKZqbaNt4Q0RaWU2U9ocCAI+6wQeBEeABDEYg+nGWksHXj/wT3wMZMQMERNtaml9v+yO0eCb7/nuUgRMjCT97w7xB0XVjCRGiAwfgz1xBFvz508dE6o5E00utxNB120tEaRdcTxhEL/4A4ct6wbR5Qb/kblEYAYb8logsYoJ375qruDxdfB06VIJU8RF0bsLvgNSbobWdPFEMeyd4/E/6gculfQHkfKMtfIzzxNCBfMFIfMFscs2i1OnK0HiSV84AtEQTvUD8YuqDXMdfuHM1rKz9gNzCcSa54wURuIgdMWAirSCFDt87RL3Yym02YMygSN0YimcaxKvFaEIYfUH4e4AhalcTxBe/8MFzldIfxDXRwQiIQUutTVo/uBmrfaQlMd72mpBvMwnLyyEqFsy5e21VBdk1twTH/gX4aTqvU7fbLwIF0q46s4hYhkhQhXubH/LgvwFhRpRILDC/3NEyvvGOad2lp/yQgXRu/OljfV4/wXvPFHLtD7zX04yUd2C4f9LHy7MR9jzpv9zrmYE7vaVO1BpxG0oFHbYBXF9Bm1kVAQQQX5AARTIYCA/P4LgOdjQ4cE/P34wfFjR4kE0eQyK0YMnjBg0azTCuYNGTBg8eeAAAmMmjB89VMyAwXOnpRQxYsAEwgNGzJlAVHKeuYNEzUEpB6ls9COTph6fUe4YxENEIhU9BQ/6MWMVSkMhP9ZcfAhH4kU0EiWiucjkh5mLVH4krSjmBxE+Dc/8EJKXbNqFB/Hc/UP2opQfOw03vBMWycqGf9BYReLHIZ+weirmsQrXIUuJTOAUPggIj9wdTDQ3TBh4scGIE18vzmgQDf+cp1L0wAXjBwqf0yrhwOF5Jg/cP2DsjCUaCExyPHq+OjFI/etBKGbM6KHufM9OPWK2B2pO1a1CJj7FSLH6A8nUg2Gh5KRfP6dlg2Z/kJW7tu3buHag66E/3JJCK0DcUowsP5DYYcHBErNvKMOk2AGJCe3TSK+wfkhPOzDOMwK+hjD7YbWHzLiLooYAEaNDJFqaiQmBBkJxK4VYXCw2nDLMyQytDKstEDHwgGKNNYgDI7wEkbxDpTUY+kMMlQIBhAo7iEODITD2gCKksZYKxLqGovDDD0C6A4OPsfgAA4yCSIoMDCJ22EGtO40QA7+DgrhTLUABRVG/IAk8jy2LmNj/wbOK5BqwLIEQ1U/Hi+zCi6pAJbJzB4LIsjDTTIlzCA63NrVToSgoNQizHW5s0S0mSHPojihq1FQiJM4odKsdXHvtjz9BVYsIPv+Cg00xiLzjD8vgSDUoPf74QyU8zJAMjjyk8AOOM+LQkkuu7GA2kCmqC4QJPgcEI48G92gTDCT9AIPEg/hAAwwp8oW3WKXy9ffff/3iKd9dHTpNClHrQvgiM6RgVOHnAmkYztdiWjgQPQAGWIyCG2pYY41VtRI3f4XCo2OEqJBC4IfwyDfhz6pVWd/RLvpDCipYNuxmkAGmeDE8xLujIEDMEGMsQIAKhI+c8IApEDiODiSPHwHR/0OzO5oFpGgxjnLDoKPW4NigsVYVbyWGAFnJaDj4ne1tuOOWe26667b7brzz1nvvi6rk+2/AAxd8cMILN/zwuadFfHHGG3f8ccgjl3xyyiu3/HLMM9d8c8479/xz0EMXfXTSSzf9dNRTV3111lt3/XXYY5d9dtprt/123HPXfXfee9e8D+CDF3544os3/njkkz/ed+YhV/75PuiQY3o5uuDCC+u36IIOOrDvwgsutsjChhlMKKGEGdJX/3z223f//fbVV9989uW3/3788c9iC/7734ILORiveQM0SPKkl70sZAEH5JsB+0TQgRCIIIIT7AAERSDBEETwgg+s4AY6sP/BCoawghrc4AUryEERplCFKwzhBUOQwhJy0IMhnOEKNzDDGp7whBiMIAQjSIL6pa8GOCAiDhK4v/5xoQtLpB4dikdAz9EhfOJLIA5uUL70tW8EKFwhF1n4RTCGUYxjJGMZadjFHJpRjWnsoApJMAISzKAOUNxcF0pgQhSykYxeVGMf/fhHQAZSkGp8oAi8QMfMbQGEHOTjIB35yEDqEZKTDGQNEOm8h8jhjjE0ISU9+UlQhpKSHCQBHS7JuD50wQZyOAgddNBJUcZSlrOk5R65cErE1cEGDySBDvqQBRKAUISSrGUxjXlMQN4Al4bbwgwKKQIScOGVH0RmNa15TT//GnKZhePCB58pgk12EZvjJOc1TbDNwm0hmLAM5AM58E54xlOe5WThPOsJTxWaMJ5/ZGQjjSkCZaJzcFxY5yALqQABCGAIQ0joQhvK0ARwgJ4trCBCE3qAD7CzAxpIqAAO4AEuPpCjF80oIP35TxL0QaCBqwNBNRrIAwgrUAfgwEmRWciN3goDXuRATDW1UxGKoKdqOUAfL7AApCpAo8QUZSlXCjgvlNCgD/SpTCVCU5ses4Qe4IFaFEDNEHogWD9Qago/EKwLZFWFGVDLEF5604A+dW99KIEnq7qDheZVrwqoaT5P+lJYxtCbYNUhTlv4wqFKZAh99eYFfnCqHyw2/4WOlUgPPOBNPAZVhxVkq2KFqVZQbvCWctVbH2bgSC5WFauc3OoDXHvZDnDAAw3AbGxny4AGeECinXygB1z7AJDG9gG4/QBhhUtcakZQA3h6QWYV8Fi18OAFKXzuVQf7zuEy4AHvrO0HOxvZRYI2tAAlrd6mKcgbilC1u2XnBhXwJ5pC4AB4lSgHMLCAHgRqCBqQqDcTS1YOXGAIeDrAAz7IAQ0MmKgGviBX1aKBHXJAwYCCcAglrJa0dsADGpgvoHZwAAzoEAMcVksPNHDiC/R3j6MUQQ7KmzcbPDKH6+3iBhNA1At0NbIc+MCEQbWAml7wvwn4LqB6sN0bZ+rIQv+u6gIw+4A/LQC+IoRyZS+bY5nuoMIdWIBMLSteTz7QBi/GWxdI8Mn1flOCIEzyD3qg4x339E874MGcJZIB/1a1zpm60wGWKyysbvRPPcjoAzvLAwwomNAh/PMPiioCKN+JznuO7gMe2GVhfXmMNWSqGS94SDLXrQ51RTNRPyBPfH6wzSUeApA7gAEeHOACwH2ABvKr2CD/t7IHOMCt8eRmXvcgWDtorgjE+lMm47qzOwgxVTFsQvwmAAPAvS+g8LzRBEyYBwngtgKKK8b0ejKlobZbjEt9K1At9oHV1VQGLrtBDhgYsyKg7GNBKtSqRpbBHsi3o2HLb0ClFd+P3cH/tT+g469iQC14FsEHbv3lBpfUwqrF43fdKlVrloDcdaPDmc9tVXWLgN0/YDgnYxvPF8C52P89MlhfEKwhvPuDL184BxutboVLZKccuDXOiTpYC8czA39y6wktLtiMb5xuowZzCpnaYZAHuc2ExqOQMZBtH2TdB8JWy3Rj2+TN9tirm73wncP6Jx5ctrqWrSCmeWDg7wr8gR/g8BC0/ua2SpCzbS2nCGag9Lm9suk09KA/9cxrxCM+ousmapA3+IF+g8rr//0qCstOcmFePgOF/G+KFfxoETTa81bmIKytCt4QWrycIcgC4OFWhywIk5Ix7bOKw13C917V8Q+c+gG4/50AKXe9gpSHpeaBbnxvaiDKHnhwIY+9gwUc29Fh9XWrfz/hIYBV9eQUwWhd/xouYHz2jRe/CHPv6L729uHybjClJ19VpVp+wpsXoYT/dO0KSn8ISU475xW8/z/JsNBTC1djNOgqur1TrL6bAZX6vsWQg8FTIxoDI3ajKRF6gca7wGB5v7GTv5oDK+QbPp+iM1zrJHbTMYjrAHbbsorKu9TjO82KQVm6IBKYIwe8CB0wt1CawC9qMwv0JppzM3nTMB/jQIn4KhrSPC4KwQQElLLaoJwDlKLSoO/6wVeDORT6LmbjgAZogB6qJhLoghu8CLqKQAnMQDDyQRWLrQnrgf8MyIAEgDOJMEKyIizk4yAm1LCx+oGdwiOeCxS5E4EodLQ37DCiQ6FB1JRHs6FZwoExvAhFMkMz4kEWSrIPU7ELajQPGys6rDwLm78cykNdG4JC0yFMszId0jVAgTMEFCofqyyJG6ZwY6FZ5KcteMSK6Li3giQRoMQVOj8rTD05/LAc+5Pm+jqviiHj80CzS6Eic7IWykQppCbngzqJ4IE3PMQd8oAhsLMl6yNJ3CynwkWHiMRwDCMoxAB1DLEweoF1bDYv2rAFIDIIiK13DLJXW8fpKiH7Wsd7A6F3hK0W6kd19DqK+oCAlCp3wgAFmEcNACkPeMeqEyqGJDIMwMf/YtqgcyLHhugDE5C9UdohkGQhEuquHQIrNesn1iq/lTSuECIhkGTJqps3E4LJlQyvkYwlENIBjmwIptvFqYqhFxojkwuqDXqhmxws1vKriTQsikI6kQwpNbPJ7lqkeSvJc8wmGgS1ngyEjhsBHTABj8vKiYKhsjxLotygEsgBU+rKApoeg6iDj0RLuqxLUaLBLWhAt6yIMiRLu/zLvywkR9xLMtwlvwRMxETLCyoBvSTMh+gDLzCBxJxMymQhnHQxx7QIL/C4yuzMyUyfHOgCOQAgLvC+zGxLOagDHDhMz2zNWPKlxsxMh8ABOaCDO7oj1zymTsvNEGJA2XwIKVIn/xPops3SKhcyS6I8SZEcSr9izjbaNFoSr5gUP8BiJBEazN+My02iQWxyoS+MyqUsyqYsJJuEJhI4T/REzxIwAfZsT/d8T/iMT/mcT/dMT/s8z6QMz28ySZTsgHHMzkBQJOU0zqM8SZysSpo8z/W8gRvQgQRSou9pIuBBHeHhHuqZniXK0C7YAvMJLMtcpBoE0D4QvJx0pN1cyXWiQftkzxpo0Cy4numhgzqITToKnyNi0BoAIhW9z/OcgS5YzRIIoN+MvfITo0ZCURZlUCNKIO1ZIi9oIu6Z0eBxS+kRUsOATC+4AVb6TcPsJ5pkLfS5gRx40QitTSeyQQBFnCyg0TauHLWoTFESqAEd2IInlVE2TVM8dR5n+jQ6yALr8YI7zVNBtZzNbD2DaMtBTdTN8QJfUtQXCwgAOw=="),
                    titlePanel(i18n$t(paste0("FroGIS v.", version))),
                    tags$ul(
                        tags$li(
                            HTML('<a href="https://e.sggw.pl/course/view.php?id=1709" target="_blank">E-learning</a>')
                        ),
                        tags$li(
                            HTML('<a href="https://www.interreg-central.eu/Content.Node/FramWat.html" target="_blank">FramWat</a>')
                        ),
                        tags$li(
                            HTML('<a href="https://drive.google.com/open?id=1iRxxHOCNuDdSB1tqBhnXtNLf14mr0S2Vl60tr8AGUc4" target="_blank">Methodology</a>')
                        ),
                        tags$li(
                            HTML('<a href="https://drive.google.com/open?id=160XyXj2tB3OP3fNdvU7aKN3xBRWOb0VAGepk-qKOz1U" target="_blank">Manual</a>')
                        ),
                        tags$li(
                            tags$a(href="https://drive.google.com/open?id=1U7ldyZMqgrJrnHQN4NW1ODzUwjBUz3J-", "Example data", `target`="_blank")
                        ),
                        tags$li(
                            tags$a(href="https://drive.google.com/open?id=1Ci4d_HMaJFh8Ko7WupFsWo-PJS8j2B18Nw6SeujIBxc", "Changelog", `target`="_blank")
                        ),
                        getOutputConsoleButton()
                    )
                )
            ),
            tags$div(class = "top-spacer"),
            tags$nav(class = "nav-progress",
                tags$ul(
                    tags$li(
                        tags$div(""),
                        tags$span("1", class = "number"),
                        tags$span("INPUT VALORIZATION GOALS", class = "title"),
                        class = "active",
                        id = "nav-progress-1"
                    ),
                    tags$li(
                        tags$div(""),
                        tags$span("2", class = "number"),
                        tags$span("CORRELATION MATRIX", class = "title"),
                        id = "nav-progress-2"
                    ),
                    tags$li(
                        tags$div(""),
                        tags$span("3", class = "number"),
                        tags$span("FINAL REPORT", class = "title"),
                        id = "nav-progress-3"
                    )
                )
            ),
            getOutputConsole(),
            bsCollapse(
                id = "pageDebugCollapse",
                open = "Indicator values (DEBUG)",
                bsCollapsePanel(
                    "Indicator values",
                    uiOutput("indicatorValuesUI")
                )
            ),
            bsCollapse(
                id = "page1Collapse",
                open = "Spacial planning units",
                bsCollapsePanel(
                    "Spacial planning units",
                    div(
                        list(
                            selectInput("spu",
                                label = i18n$t("SPU"),
                                choices = c(
                                    "...",
                                    "elementary basins",
                                    "water bodies",
                                    "aggregated water bodies",
                                    "Hydrologic Response Unit"),
                                selected = "..."),
                            bsTooltip("spu", "Zip file with compressed directory containing shape files",
                            "right", options = list(container = "body"))
                        ),
                        class = "framwat-input-field input-field-spu"
                    ),
                    div(
                        list(
                            fileInput("border_file", "Custom border", #()
                                multiple = FALSE,
                                accept = c(
                                    "application/zip",
                                    ".zip"
                                )
                            ),
                            bsTooltip("border_file", "Zip file with compressed directory containing shape files",
                                "right", options = list(container = "body"))
                        ),
                        class = "framwat-input-field input-field-border-file"
                    )
                ),
                bsConditionalCollapsePanel(
                    "Goals and indicators",
                    list(
                        div(
                            selectInput("goal",
                                label = i18n$t("Choose goal"),
                                    choices = c(
                                        "general",
                                        "drought",
                                        "flood",
                                        "water quality",
                                        "sediment transport"),
                                selected = "general",
                            ),
                            class = "framwat-input-field"
                        ),
                        div(
                            list(
                                tags$label("Legend"),
                                tags$p(
                                    list(
                                        tags$span("Recommended", class="label label-primary"),
                                        "usually good quality data available and easy to calculate",
                                        tags$br(),
                                        tags$span("Necessary", class="label label-necessity-n"),
                                        getDescriptionForNecessityType("N"),
                                        tags$br(),
                                        tags$span("Possible", class="label label-necessity-p"),
                                        getDescriptionForNecessityType("P"),
                                        tags$br(),
                                        tags$b("Remember to always choose both types of indicators that represent needs and possibility")
                                    )
                                )
                            ),
                            class = "necessity-legend"
                        ),
                        uiOutput("indicatorsUI")
                    ),
                    condition = "output.isIndicatorsPanelVisible == 'TRUE'"
                ),
                bsConditionalCollapsePanel(
                    "Data input",
                    uiOutput("inputsUI"),
                    condition = "output.isInputsPanelVisible == 'TRUE'"
                ),
                bsCollapsePanel(
                    "Indicators correlation matrix",
                    uiOutput("correlationMatrixUI")
                ),
                bsCollapsePanel(
                    "Conversion and final aggregation method",
                    list(
                        tags$p("Please choose the number of classes and if needed adjust their ranges. Changes in the direction of influence (stimulation or not-stimulation) of the indicator value on the need for water retention should be done only in exceptional situations. Changes in the weight are recommended if a clear dominance of one group of indicators, eg concerning LandUse was observed, then you can reduce their importance by changing the weight."),
                        div(
                            selectInput("indicatorNumClasses",
                                label = "Number of classes",
                                choices = list("0" <- "choose", 3, 4, 5),
                                selected = "0"),
                            selectInput("indicatorMethodClasses",
                                label = "Class division method",
                                choices = list("equal width" = "equal", "natural breaks" = "jenks", "quantile" = "quantile"),
                                selected = "equal"),
                            class = "framwat-input-field input-field-indicator-num-classes"
                        ),
                        uiOutput("indicatorsSummaryUI")
                    )
                ),
                bsConditionalCollapsePanel(
                    "Goal valorization result",
                    list(
                        div(
                            selectInput("goalNumClasses",
                                label = "Number of classes",
                                choices = list("0" <- "choose", 3, 4, 5, 6, 7, 8, 9, 10),
                            selected = "0"),
                            selectInput("goalMethodClasses",
                                label = "Class division method",
                                choices = list("equal width" = "equal", "natural breaks" = "jenks", "quantile" = "quantile"),
                                selected = "equal"),
                            class = "framwat-input-field input-field-goal-num-classes"
                        ),
                        uiOutput("goalSummaryUI"),
                        div("Valorization map of possibilities and needs of water retention", class="goal-map-heading"),
                        div("The map presents the final classification obtained by the aggregation of individual classes of indicators divided into a selected number of classes. Min class represents low potential for water retention and max class high.", class="goal-map-description"),
                        leafletOutput("resultMap")
                    ),
                    condition = "output.isGoalPanelVisible == 'TRUE'"
                )
            )#,
            #actionButton("gotoPrevPage", "Back"),
            #actionButton("gotoNextPage", "Process")
        )
    )
}