Configuration = setRefClass(
    "Configuration",
    fields = c("appData", "indicatorsForAllInputs", "calculationFunctions"),
    methods = list(
        initialize = function(data) {
            appData <<- data
            indicatorsForAllInputs <<- getIndicatorsForAllInputs()
            calculationFunctions <<- getAllIndicatorCalculationFunctions()
        },
        getIndicatorsForGoal = function(goal) {
            foundIndicators = list()

            for (indicatorId in attributes(appData$indicators)$names) {
                indicator = appData$indicators[[indicatorId]]

                if (is.element(goal, indicator$goal)) {
                    foundIndicators[[indicatorId]] <- indicator
                }
            }

            return(foundIndicators)
        },
        getRecommendedForGoal = function(goal) {
            return(appData$recommended[[goal]])
        },
        getIndicators = function() {
            return(appData$indicators)
        },
        getIndicatorIds = function() {
            return(attributes(appData$indicators)$names)
        },
        getDataInputIds = function() {
            return(attributes(appData$inputs)$names)
        },
        getDataInputs = function() {
            return(appData$inputs)
        },
        getDataInputsByIds = function(ids) {
            inputs = list()

            for (input in ids) {
                if (input != "SubBasin") {
                    input = strsplit(input, "$", fixed = TRUE)[[1]][[1]]

                    inputs[[input]] <- appData$inputs[[input]]
                }
            }

            return(inputs)
        },
        getDataInputById = function(id) {
            return(appData$inputs[[id]])
        },
        getDataInputsForIndicators = function(indicators) {
            inputs = list()

            for (indicator in indicators) {
                inputs = list(inputs, list(appData$indicators[[indicator]]$input))
            }

            inputs = unique(unlist(inputs))

            return(inputs)
        },
        getDataInputsForIndicator = function(indicator) {
            return(appData$indicators[[indicator]]$input)
        },
        getCalculationFunctionByIndicator = function(indicatorId) {
            indicator = appData$indicators[[indicatorId]]

            if (!is.null(indicator$calculation)) {
                return(indicator$calculation)
            }

            return("")
        },
        getAllIndicatorCalculationFunctions = function() {
            functions = list()

            for (id in getIndicatorIds()) {
                indicator = appData$indicators[[id]]

                if (!is.null(indicator$calculation)) {
                    functions[[length(functions) + 1]] = indicator$calculation
                }
            }

            return(functions)
        },
        getIndicatorsForAllInputs = function() {
            inputs = list()

            for (indicatorId in getIndicatorIds()) {
                inputs_ = getDataInputsForIndicators(list(indicatorId))

                for (input in inputs_) {
                    if (is.null(inputs[[input]])) {
                        inputs[[input]] = indicatorId
                    } else {
                        inputs[[input]] = list(inputs[[input]], list(indicatorId))
                    }

                    inputs[[input]] = unlist(inputs[[input]])
                }
            }

            return(inputs)
        },
        getIndicatorsForDataInput = function(inputId) {
            return(indicatorsForAllInputs[[inputId]])
        },
        getGlobalDataDSN = function() {
            config = appData$globalDB

            return(paste0(
                "PG:host=", config$host,
                " port=", config$port,
                " dbname=", config$dbname,
                " user=", config$user,
                " password=", config$password)
            )
        }
    )
)