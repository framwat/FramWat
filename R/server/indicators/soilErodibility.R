soilErodibilityCalculation <- function(SPU, sand, silt, clay, orgC) {

    #Find raster witch worst resolusion
    list_r = c(xres(sand),xres(silt),xres(clay),xres(orgC))
    resMax <- list_r[[1]]
    for(i in 2:4) {
        if (list_r[[i]] > resMax) {
            resMax <- list_r[[i]]}
    }
    mapMax <- sand
    if (xres(silt) == resMax) {
        mapMax <- silt
    } else if (xres(clay) == resMax) {
        mapMax <- clay
    } else if (xres(orgC) == resMax) {
        mapMax <- orgC
    }
    ras <- raster(res=resMax, ext=extent(mapMax), crs= crs(mapMax))
    beginCluster()
        sand=resample(sand, ras, method="bilinear")
        silt=resample(silt, ras, method="bilinear")
        clay=resample(clay, ras, method="bilinear")
        orgC=resample(orgC, ras, method="bilinear")
    endCluster()

    # See SWAT I/O documentation for the description of the function
    fcsand=(0.2+0.3*exp(-0.256*sand*(1-silt/100)))
    fcl_hi=(silt/(clay+silt))^0.3
    forgc=1-(0.0256*orgC/(orgC+exp(3.72-2.95*orgC)))
    s=1-sand/100
    fhisand=1-(0.7*s/(s+exp(-5.51+22.9*s)))

    uslek=fcsand*fcl_hi*forgc*fhisand

    beginCluster()
        k <- raster::extract(uslek, SPU, fun = median, na.rm = T, small = T, df = T)
    endCluster()

    return(list("values"=k[,2]))
}
