onePolyToSPUCalculation <- function(SPU, shpIn, weightName, weight) {
    shpIn$areaIn <- abs(area(shpIn))
    dfRatio <- data.frame(shpIn)

    # fallback for dealing with integers interpreted as strings
    if (is.factor(weight)) {
        weight<-as.numeric(as.factor(weight))
    }

    dfRatio['indXareaIn'] <- shpIn$areaIn * weight
    aRatio <- raster::aggregate(dfRatio[,c("indXareaIn"), drop=FALSE],
        dfRatio[,c("ID"), drop=FALSE],
        FUN="sum"
    )

    dfSPU = data.frame(SPU)
    mRatio <- raster::merge(
        dfSPU[,c("ID", "AreaSPU"), drop=FALSE],
        aRatio,
        by="ID",
        all.x=TRUE
    )

    return(list("values" = mRatio$indXareaIn / mRatio$AreaSPU))
}