densityCalculation <- function(SPU, Density_I){

    Density_I$area <- abs(area(Density_I))#counting the area
    densityDF <- data.frame(Density_I)

    a_density <- raster::aggregate(densityDF[,c('area'), drop=FALSE],
                                densityDF[,c('ID'), drop=FALSE],
                                FUN="sum") # aggregating and summing up the areas

    bdf = data.frame(SPU)

    m_density <- merge(
        bdf[,c('ID', 'AreaSPU'), drop=FALSE],
        a_density,
        by='ID',
        all.x=TRUE
    ) # merging the data together

    percentage <- 100 * m_density$area / m_density$AreaSPU #counting %

    percentage[is.na(percentage)] <- 0

    return(percentage)
}