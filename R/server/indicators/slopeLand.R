slopeLandCalculation=function(SPU, DEM) {
    #Calculating slope for the further analysis
    slope <- raster::terrain(DEM, opt='slope', unit='degrees', neighbors=8, overwrite=TRUE)

    beginCluster() # run parallel processing for 4 core
        result <- raster::extract(slope, SPU, fun = function(x) {mean(x, na.rm = TRUE)})  # if not use beginClusetr format is "extract(slope, SPU, fun = mean, na.rm = T)"
    endCluster() # end parallel processing sesion

    return(result[,1])
}
