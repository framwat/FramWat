ratioCalculation <- function(SPU, shpIn, numName, numTor, denName, denTor){

    shpIn$areaIn <- abs(area(shpIn))#calculate the area
    dfRatio <- data.frame(shpIn)
    # calculation of partial ratio for interect polygons
    dfRatio["indXareaIn"] <- numTor / denTor * dfRatio$areaIn
    # aggregating and summing up the areas by ID column
    aRatio <- raster::aggregate(
        dfRatio[,c('indXareaIn'), drop=FALSE],
        dfRatio[,c('ID'), drop=FALSE],
        FUN="sum"
    )

    # merging the data together
    dfSPU = data.frame(SPU)
    mRatio <- merge(
        dfSPU[,c('ID', 'AreaSPU'), drop=FALSE],
        aRatio,
        by='ID',
        all.x=TRUE
    )

    return(mRatio$indXareaIn / mRatio$AreaSPU)
}