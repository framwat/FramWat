ecoBadRHSCalculation<-function(SPU, River_I, BadRHS_I){

    #calculating length
    SpatialLinesLengths(River_I) -> River_I$length_R
    SpatialLinesLengths(BadRHS_I) -> BadRHS_I$length_H

    #convert to data frame
    df_SPU = data.frame(SPU)
    River_I <- data.frame(River_I)
    BadRHS_I <- data.frame(BadRHS_I)

    #aggregating and summing up length
    a_River <- raster::aggregate(River_I[,c('length_R'), drop=FALSE],
    River_I[,c('ID'), drop=FALSE],
    FUN="sum") # aggregating badRHS
    a_BadRHS <- raster::aggregate(BadRHS_I[,c('length_H'), drop=FALSE],
    BadRHS_I[,c('ID'), drop=FALSE],
    FUN="sum") # aggregating badRHS


    #merging by ID
    m_River <- merge(
    df_SPU[,c("ID"), drop=FALSE],
    a_River,
    by='ID',all.x=TRUE) # merging the data together

    m_All <- merge(
    m_River[,c("ID","length_R"), drop=FALSE],
    a_BadRHS,
    by='ID',all.x=TRUE) # merging the data together

    m_All$ind <- m_All$length_H/m_All$length_R*100
    m_All$ind[is.na(m_All$ind)] <- 0

    return(list("values" = m_All$ind))
}

