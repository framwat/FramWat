removeIndicator <- function(indicatorId) {

    appData$selectedIndicators[[indicatorId]] <- NULL

    inputs <- configuration$getDataInputsForIndicators(c(indicatorId))
    allInputs <- configuration$getDataInputsForIndicators(attributes(appData$selectedIndicators)$names)

    diffs = setdiff(inputs, allInputs)

    if (length(appData$inputs) == 0) {
        return()
    }

    for (d in diffs) {
        if (length(appData$inputs) > 0) {
            for (i in 1:length(appData$inputs)) {
                if (i <= length(appData$inputs)) {
                    if (appData$inputs[[i]] == d) {
                        appData$inputs[[i]] <- NULL
                    }
                }
            }
        }
    }
}