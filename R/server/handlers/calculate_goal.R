calculateGoal <- function() {
    numClasses = appData$goalNumClasses

    appData$goalValues = list()

    indicators = intersect(attributes(appData$indicatorWeights)$names, attributes(appData$selectedCorrelatedIndicators)$names)

    for (indicator in indicators) {
        if (!is.null(appData$indicatorsAsClasses[[indicator]]) && !is.null(appData$indicatorWeights[[indicator]])) {
            weight = appData$indicatorWeights[[indicator]]

            if (length(appData$goalValues) == 0) {
                appData$goalValues <- appData$indicatorsAsClasses[[indicator]] * weight
            } else {
                appData$goalValues <- appData$goalValues + appData$indicatorsAsClasses[[indicator]] * weight
            }
        }
    }

    if (length(appData$goalValues) == 0) {
        setActiveNavBar(2, TRUE)

        form$isGoalPanelVisible = "FALSE"

        return()
    }

    setActiveNavBar(3, FALSE)

    form$isGoalPanelVisible = "TRUE"

    stats = summary(appData$goalValues)

    form$goalSummary <- getGoalResultUI(appData$goalValues)

    if(numClasses == 0 || length(appData$goalClassRanges) == 0 || length(appData$goalClassRanges) < numClasses || length(appData$goalClassRanges[1:(numClasses-1)]) < numClasses-1) {
        return()
    }

    ranges = c(appData$goalClassRanges[1:numClasses], stats$max)
    ranges[[1]] = stats$min

    logdebug("RANGES")
    logdebug(ranges)

    if(!isMonotonousAndAscending(ranges)) {
        return()
    }

    appData$goalValues <- unlist(appData$goalValues)

    values <- appData$goalValues
    values[values > stats$max] = stats$max
    values[values < stats$min] = stats$min

    appData$goalAsClasses = as.numeric(
        cut(
            values,
            breaks = unlist(ranges),
            right = FALSE,
            include.lowest = TRUE,
            labels = seq(1,numClasses,1)
        )
    )

    transformedMap <- spTransform(appData$borderMap, CRS("+proj=longlat +datum=WGS84"))

    centers <- gCentroid(transformedMap, byid = TRUE)

    labelsOnlySpuID <- sprintf("#%s", transformedMap@data$ID) %>% lapply(htmltools::HTML)

    labelHints <- transformedMap@data$ID

    genHint <- function(spuId) {
        re = paste0("<strong>SPU: #", spuId, "</strong><br/><table><tr><th>indicator</th><th>value</th><th>class</th></tr>")

        for (indicatorId in attributes(appData$indicators)$names) {
            re = paste0(
                re,
                "<tr><td>",
                indicatorId,
                "</td><td>",
                round(appData$indicators[[indicatorId]][[spuId]], 3),
                "</td><td>",
                appData$indicatorsAsClasses[[indicatorId]][[spuId]],
                "</td></tr>"
            )
        }

        re = paste0(
            re,
            "<tr><td>Goal</td><td>",
            round(appData$goalValues[[spuId]], 3),
            "</td><td>",
            appData$goalAsClasses[[spuId]],
            "</td></tr>"
        )

        paste0(re, "</table>")

        return(htmltools::HTML(re))
    }

    labels <- lapply(labelHints, genHint)

    palv <- colorFactor("Greens", 1:appData$goalNumClasses)

    map <- leaflet(transformedMap)

    map <- addPolygons(map, color = "#333333", weight = 1, smoothFactor = 0.5,
        fillColor = ~palv(appData$goalAsClasses),
        opacity = 1.0, fillOpacity = 0.5, label = labels,
        highlightOptions = highlightOptions(color = "white", weight = 2,
        bringToFront = TRUE))

    nullIcon <- makeIcon(
        iconUrl = "base64:iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=",
        iconWidth = 0, iconHeight = 0,
        iconAnchorX = 0, iconAnchorY = 0,
        shadowUrl = "base64:iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=",
        shadowWidth = 0, shadowHeight = 0,
        shadowAnchorX = 0, shadowAnchorY = 0
    )

    map <- addMarkers(
        map,
        icon = nullIcon,
        data = coordinates(centers),
        label = labelsOnlySpuID,
        labelOptions = labelOptions(noHide = T, textOnly = TRUE))

    map <- addTiles(map)
    map <- addLegend(
        map,
        title = "Classes",
        pal = palv,
        values = ~appData$goalAsClasses,
        opacity = 1.0
    )

    map <- suspendScroll(map)

    form$resultMap <- map
}