
selectedPage <- 1
selectedPane <- "Spacial planning units"

source("R/ui/indicators.R")
source("R/ui/inputs.R")
source("R/ui/layout.R")
source("R/ui/correlation_matrix.R")
source("R/ui/indicators_summary.R")
source("R/ui/indicator_values.R")
source("R/ui/goal_result.R")
source("R/ui/utils.R")

ui <- fluidPage(
    useShinyjs(),
    includeCSS("www/style.css"),
    includeScript("www/script.js"),
    getLayout(getAppVersion(), isDebug)
)