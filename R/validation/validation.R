FileProcessor = setRefClass(
    "FileProcessor",
    fields = c("sessionId"),
    methods = list(
        getFilePath = function(sessionId, fileId) {
            return(paste0("/var/tmp/framwat/", fileId, "/", sessionId))
        },
        processZippedFile = function(path) {
            file <- list()

            file$fileId <- hashGenerator(maxVal = 1)
            file$dirPath <- getFilePath(file$fileId, sessionId)

            dir.create(file$dirPath)
            unzip(path, exdir = file$dirPath, junkpaths = TRUE)

            return(file$dirPath)
        },
        processRasterFile = function(path) {
            file <- list()

            file$fileId <- hashGenerator(maxVal = 1)
            file$dirPath <- getFilePath(file$fileId, sessionId)

            fullPath = paste0(file$dirPath,"/",basename(path))

            dir.create(file$dirPath)
            file.copy(path, fullPath)

            return(fullPath)
        }
    )
)

MapValidator = setRefClass(
    "MapValidator",
    methods = list(
        validateMap = function(map, fileType, format) {
            if (fileType == "shapefile") {
                if (format == "polygon") {
                    if (isMapSpacialPolygons(map) && hasProjectionValidator(map) && hasAtLeastOneFeatureValidator(map) && areaOfFeaturesArePositive(map)) {
                        return(TRUE)
                    }
                } else if (format == "polyline") {
                    if (isMapSpacialLines(map) && hasAtLeastOneFeatureValidator(map) && lengthOfFeaturesArePositive(map)) {
                        return(TRUE)
                    }
                } else if (format == "point") {
                    if(isMapSpacialPoints(map) && hasAtLeastOneFeatureValidator(map)) {
                        return(TRUE)
                    }
                }
                return(FALSE)
            }

            return(validateRasterMap(map))
        },
        validateRasterMap = function(map) {
            return(TRUE)
        },
        validateVectorMap = function(map) {
            #hasProjectionValidator(map)
        },
        properShapefileValidator = function(map) {
            err <- NULL

            tryCatch({
                readOGR(dsn=map)
            }, warning = function(w) {
                logdebug(w$message)
            }, error = function(e) {
                logdebug(e$message)
                err <<- e$message
            })

            return(err)
        },
        hasProjectionValidator = function(map) {
            print("hasProjectionValidator")
            print(map)

            projection = crs(map)

            if (is.na(projection)) {
                return(FALSE)
            }

            return(TRUE)
        },
        hasAtLeastOneFeatureValidator = function(map) {
            return(length(map) > 0)
        },
        lengthOfFeaturesArePositive = function(map) {
            lengths = sp::SpatialLinesLengths(as(map, "SpatialLines"))

            for (i in 1:length(lengths)) {
                if (lengths[[i]] <= 0) {
                    return(FALSE)
                }
            }

            return(TRUE)
        },
        areaOfFeaturesArePositive = function(map) {
            map$area <- abs(area(map))

            for (i in 1:length(map$area)) {
                if (map$area[[i]] <= 0) {
                    return(FALSE)
                }
            }

            return(TRUE)
        },
        hasOnlyOneAttribute = function() {

        },
        isMapSpacialPolygons = function(map) {
            print(is(map, "SpatialPolygonsDataFrame"))
            return(is(map, "SpatialPolygonsDataFrame"))
        },
        isMapSpacialLines = function(map) {
            return(is(map, "SpatialLinesDataFrame"))
        },
        isMapSpacialPoints = function(map) {
            return(is(map, "SpatialPointsDataFrame"))
        }
    )
)