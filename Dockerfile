FROM registry.gitlab.com/framwat/framwat/debian/r-stretch:3.3.3
WORKDIR /usr/local/src/framwat
COPY install.R /usr/local/src/framwat
COPY . /usr/local/src/framwat
EXPOSE 8080
CMD ["Rscript", "app.R", "--no-save", "--debug"]
