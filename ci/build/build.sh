#!/bin/sh
cd kibana-nginx
docker build . -t registry.gitlab.com/framwat/framwat/kibana-nginx:latest
docker push registry.gitlab.com/framwat/framwat/kibana-nginx:latest
cd ././postgres-postgis
docker build . -t registry.gitlab.com/framwat/framwat/postgres-postgis:latest
docker push registry.gitlab.com/framwat/framwat/postgres-postgis:latest